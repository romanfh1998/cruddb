package edu.pucmm.jdbc.models;

import spark.Route;

/**
 * @author a.marte
 */
public abstract class RouteBase implements Route {

    public String path;

    protected RouteBase(String path) {
        this.path = path;
    }
}
