package edu.pucmm.jdbc.routes.students;

import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.DB;
import java.sql.*;
import java.text.SimpleDateFormat;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class EditStudent extends RouteBase {

    public EditStudent() {
        super("/students/edit");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        String idstr = request.queryParams("id");
        String names = request.queryParams("names");
        String lastnames = request.queryParams("lastnames");
        String birdthStr = request.queryParams("birdth");
        if (idstr != null && names != null && lastnames != null && birdthStr != null
                && !idstr.isEmpty() && !names.isEmpty() && !lastnames.isEmpty() && !birdthStr.isEmpty()) {
             int id = Integer.parseInt(idstr);
             SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        java.util.Date parsed = format.parse(birdthStr);
        java.sql.Date birdth = new java.sql.Date(parsed.getTime());
         
                    
                     try{

                  
            PreparedStatement ps = DB.getInstance().getPreparedStatement("UPDATE students  SET names=?, lastnames=? ,birthDate=? WHERE  id=?");
            ps.setInt(4,id);     
            ps.setString(1,names);
            ps.setString(2,lastnames);
            ps.setDate(3,  birdth); 
            
            int rowsUpdated = ps.executeUpdate();
            System.out.println(rowsUpdated);
            }   
        catch (Exception e) 
                    {
                
                 e.printStackTrace();
            }
           
           return "1";
        }
        return "0";
    }
}


