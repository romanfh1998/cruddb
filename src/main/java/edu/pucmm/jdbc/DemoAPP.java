package edu.pucmm.jdbc;

import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.routes.IndexRoute;
import edu.pucmm.jdbc.routes.students.DeleteStudent;
import edu.pucmm.jdbc.routes.students.EditStudent;
import edu.pucmm.jdbc.routes.students.NewStudent;
import spark.Spark;

/**
 * @author a.marte
 */
public class DemoAPP {

    public static void main(String[] args) {
        Spark.port(8080);
        addGetRoute(new IndexRoute());
        addGetRoute(new NewStudent());
        addGetRoute(new EditStudent());
        addGetRoute(new DeleteStudent());
        Spark.init();
    }

    private static void addGetRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }
}
