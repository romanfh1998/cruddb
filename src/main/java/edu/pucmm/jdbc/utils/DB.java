package edu.pucmm.jdbc.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author a.marte
 */
public class DB {

    private static volatile DB instance = null;

    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:mem:dbStudents";

    private static final String USER = "demp";
    private static final String PASS = "db";

    private Connection connection = null;

    private DB() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        createTables();
    }

    private void createTables() {
        try {
            getStatement().execute("CREATE TABLE IF NOT EXISTS students "
                    + "(id INTEGER not NULL, "
                    + " names VARCHAR(255), "
                    + " lastnames VARCHAR(255), "
                    + " birthDate DATE, "
                    + " PRIMARY KEY ( id ));");
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static DB getInstance() {
        DB result = instance;
        if (result == null) {
            synchronized (DB.class) {
                if (result == null) {
                    instance = result = new DB();
                }
            }
        }
        return result;
    }

    public Statement getStatement() {
        try {
            return connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PreparedStatement getPreparedStatement(String sql) {
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Connection getConnection() {
        return connection;
    }
}
